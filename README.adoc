= Crumbs ABP (ARCHIVED)
=================


== THIS PROJECT HAS BEEN MOVED TO https://gitlab.com/eyeo/crumbs/crumbs-android[CRUMBS-ANDROID] REPO


This repository contains a Crumbs filter engine implementation based on AdBlockPlus.


== Requirements

In order to run the unit test suite or build the project you need a working java environment.
This project has been build with Android SDK.

== Setup

- Retrieve the dependency

[source,groovy]
----
repository {
    maven { url "https://gitlab.com/api/v4/groups/8621009/-/packages/maven" }
}
dependences {
    implementation "org.crumbs:crumbs-abp-android:$LAST_VERSION"
}
----

- Instantiate the filter engine

[source,kotlin]
----
val listener = ABPListener()
val engineProvider = SingleInstanceEngineProvider(
    AdblockEngine.builder(this, AdblockEngine.BASE_PATH_DIRECTORY)
)
engineProvider.addEngineCreatedListener {
    listener.setupEngine(it)
}
webView.setProvider(engineProvider)
listener.setupEventListener(webView)
----


== Developer commands

|Command|Description|
|--|--|
|`./gradlew assemble`| assemble this project |
|`./gradlew check`| test this project |
|`./gradlew build`| assemble and test this project |

== License

Licensed under CC BY-NC-ND 4.0;
you may not use this file except in compliance with the License.
You may obtain a copy of the License at https://creativecommons.org/licenses/by-nc-nd/4.0/