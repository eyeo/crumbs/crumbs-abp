package org.crumbs.abp

import org.adblockplus.libadblockplus.android.AdblockEngine
import org.adblockplus.libadblockplus.android.webview.AdblockWebView
import org.crumbs.CrumbsAndroid
import org.crumbs.CrumbsLogger
import org.crumbs.presenter.IntegrationPresenter
import org.crumbs.utils.UIExtension.getTabId
import java.lang.ref.WeakReference

@Suppress("unused")
class ABPListener {

    private val logger = CrumbsLogger("ABPListener")
    private val integration = CrumbsAndroid.get().integration()

    fun setupEventListener(
        webView: AdblockWebView,
        nestedListener: AdblockWebView.EventsListener? = null
    ) {
        webView.setEventsListener(EventsListener(webView.getTabId(), nestedListener))
    }

    fun setupEngine(engine: AdblockEngine) {
        integration.setFilterEngineConfigListener(ConfigListener(engine))
    }

    private inner class ConfigListener(engine: AdblockEngine) :
        IntegrationPresenter.FilterEngineConfigListener {

        private var lastAllowedList = emptyList<String>()

        private val engineReference: WeakReference<AdblockEngine> = WeakReference(engine)

        override fun onAllowedDomainsChange(allowedDomains: List<String>) {
            val engine = engineReference.get() ?: return
            lastAllowedList.filter { !allowedDomains.contains(it) }.forEach { domain ->
                val filter =
                    engine.filterEngine.getFilterFromText("@@||$domain^\$document,domain=$domain")
                engine.filterEngine.removeFilter(filter)
            }
            allowedDomains.filter { !lastAllowedList.contains(it) }.forEach { domain ->
                val filter =
                    engine.filterEngine.getFilterFromText("@@||$domain^\$document,domain=$domain")
                engine.filterEngine.addFilter(filter)
            }
            lastAllowedList = allowedDomains
        }

        override fun onSubscriptionsChange(subscriptionUrls: List<String>) {
            val engine = engineReference.get() ?: return
            IntegrationPresenter.SUBSCRIPTIONS.forEach {
                val subscription = engine.getSubscription(it)
                if (subscriptionUrls.contains(it)) {
                    engine.filterEngine.addSubscription(subscription)
                } else {
                    engine.filterEngine.removeSubscription(subscription)
                }
            }
        }
    }

    private inner class EventsListener(
        private val tabId: String,
        private val nestedListener: AdblockWebView.EventsListener?
    ) : AdblockWebView.EventsListener {
        override fun onNavigation() {
            nestedListener?.onNavigation()
        }

        override fun onResourceLoadingBlocked(info: AdblockWebView.EventsListener.BlockedResourceInfo?) {
            integration.onTrackerBlocked(tabId) //TODO differentiate blocked content (ads/privacy/popup...)
            nestedListener?.onResourceLoadingBlocked(info)
        }

        override fun onResourceLoadingAllowlisted(info: AdblockWebView.EventsListener.AllowlistedResourceInfo?) {
            nestedListener?.onResourceLoadingAllowlisted(info)
        }

    }
}